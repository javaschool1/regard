package ru.smlab.redard;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.smlab.redard.pages.BasePage;
import ru.smlab.redard.pages.BasketPage;
import ru.smlab.redard.pages.CardOfProductPage;
import ru.smlab.redard.pages.MainPage;

public class RegardTest extends BasePage {
    /*
    Реализовать следующий тестовый сценарий:
    1. Открыть главную страницу
    2. В левом боковом меню выбрать категорию материнские платы - в подменю выбрать Intel socket 1200
    3. Найти 5-ю в списке материнскую плату, положить ее в корзину (маленькая круглая красная кнопка около товара).
    4. В левом боковом меню выбрать категорию корпуса - в подменю выбрать Aerocool
    5. Найти 4-й в списке корпус, положить его в корзину (маленькая круглая красная кнопка около товара).
    6. Найти 10-й в списке корпус, кликнуть на ссылку-название (откроется страница товара).
    7. Нажать на красную кнопку "Добавить в корзину" справа от товара.
    8. Нажать на голубую кнопку "Перейти в корзину".
    9. Убедиться, что корзина не пустая, и что в ней содержатся ровно те наименования товаров, которые вы покупали.
     */

    @Test
    public void testAddToCart() throws InterruptedException {

        //1. Открыть главную страницу
        MainPage mainPage = new MainPage();
        mainPage.open();

        //2. В левом боковом меню выбрать категорию материнские платы - в подменю выбрать Intel socket 1200
        mainPage.clickMenuItem(MainPage.Menu.MOTHERBOARD);
        mainPage.clickSubmenuItem(MainPage.Submenu.INTELSOCKET1200);

        //3. Найти 5-ю в списке материнскую плату, положить ее в корзину (маленькая круглая красная кнопка около товара).
        mainPage.clickButtonAddInBasket(5);
        mainPage.addProductInArrayOfBasketProducts(5);

        //4. В левом боковом меню выбрать категорию корпуса - в подменю выбрать Aerocool
        mainPage.clickMenuItem(MainPage.Menu.COMPUTERCASE);
        mainPage.clickSubmenuItem(MainPage.Submenu.ACCORD);

        //5. Найти 4-й в списке корпус, положить его в корзину (маленькая круглая красная кнопка около товара).
        mainPage.clickButtonAddInBasket(4);
        mainPage.addProductInArrayOfBasketProducts(4);

        //6. Найти 10-й в списке корпус, кликнуть на ссылку-название (откроется страница товара).
        mainPage.clickNameOfProduct(10);

        //7. Нажать на красную кнопку "Добавить в корзину" справа от товара.
        CardOfProductPage cardOfProductPage = new CardOfProductPage();
        cardOfProductPage.clickButtonAddToCart();
        cardOfProductPage.addProductInArrayOfBasketProducts();

        //8. Нажать на голубую кнопку "Перейти в корзину".
        cardOfProductPage.clickButtonGoToCart();

        //9. Убедиться, что корзина не пустая, и что в ней содержатся ровно те наименования товаров, которые вы покупали.
        BasketPage basketPage = new BasketPage();
        Assert.assertTrue(!basketPage.isTextEmptyBasketDisplayed());
        Assert.assertTrue(basketPage.isBasketContainsNamesOfProducts(productsAddedInBasket));
    }

}
