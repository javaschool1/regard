package ru.smlab.redard.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class BasketPage extends BasePage {

    public String locatorTextEmptyBasket = "//h3[contains(@class,'alarm')]";
    public String locatorTextNamesOfProducts = "//table[@id='table-basket']//a[contains(@class,'underline')]";

    public boolean isTextEmptyBasketDisplayed() {
        try {
            return driver.findElement(By.xpath(locatorTextEmptyBasket)).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isBasketContainsNamesOfProducts(List<String> expectedNamesOfProducts) {
        List<String> actualNamesOfProducts = driver.findElements(By.xpath(locatorTextNamesOfProducts))
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());

        for (String expectedName : expectedNamesOfProducts) {
            boolean actualContainsInExpected =
                    actualNamesOfProducts.stream().anyMatch(actualName -> actualName.contains(expectedName));
            if (actualContainsInExpected == false) {
                return false;
            }
        }

        return true;
    }
}
