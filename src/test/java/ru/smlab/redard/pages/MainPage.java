package ru.smlab.redard.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class MainPage extends BasePage {

    public enum Menu {
        MOTHERBOARD("Материнские платы"),
        COMPUTERCASE("Корпуса");

        String name;

        Menu(String name) {
            this.name = name;
        }
    }

    public enum Submenu {
        INTELSOCKET1200("Intel Socket 1200", Menu.MOTHERBOARD),
        ACCORD("ACCORD", Menu.COMPUTERCASE);

        String name;
        Menu menuName;

        Submenu(String name, Menu menuName) {
            this.name = name;
            this.menuName = menuName;
        }
    }


    public String locatorMenuItem = "//div[@id='lmenu']//a[text()='%s']";
    public String locatorSubmenuItem = "//div[@id='lmenu']//a[text()='%s']/ancestor::li[1]//a[text()='%s']";
    public String locatorButtonAddInBasket = "(//div[@id='hits']//div[@class='block'])[%d]//a[contains(@class,'cart')]";
    public String locatorNameOfAddedProduct = "(//div[@id='hits']//div[@class='block'])[%d]//a[contains(@class,'header')]";
    public String locatorBlockOfProduct = "(//div[@id='hits']//div[@class='bcontent'])[%d]";

    public void clickMenuItem(Menu menuItem) {
        driver.findElement(By.xpath(String.format(locatorMenuItem, menuItem.name))).click();
    }

    public void clickSubmenuItem(Submenu submenuItem) {
        driver.findElement(By.xpath(String.format(locatorSubmenuItem, submenuItem.menuName.name, submenuItem.name))).click();
    }

    public void clickButtonAddInBasket(int numberOfProduct) {
        driver.findElement(By.xpath(String.format(locatorButtonAddInBasket, numberOfProduct))).click();
    }

    public void addProductInArrayOfBasketProducts(int numberOfProduct) {
        String nameOfAddedProduct = driver.findElement(By.xpath(String.format(locatorNameOfAddedProduct, numberOfProduct))).getText();
        productsAddedInBasket.add(nameOfAddedProduct);
    }

    public void clickNameOfProduct(int numberOfProduct) {
        Actions actions = new Actions(driver);
        WebElement element = driver.findElement(By.xpath(String.format(locatorBlockOfProduct, numberOfProduct)));

        actions.moveToElement(element).build().perform();
        driver.findElement(By.xpath(String.format(locatorNameOfAddedProduct, numberOfProduct))).click();
    }

}
