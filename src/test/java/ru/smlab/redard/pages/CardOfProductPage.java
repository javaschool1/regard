package ru.smlab.redard.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class CardOfProductPage extends BasePage {
    public String locatorNameOfProduct = "//div[contains(@class,'goods_header')]//span";
    public String locatorButtonAddToCart = "//a[@id='add_cart'][contains(@title,'Добавить в корзину')]";
    public String locatorButtonGoToCart = "//a[@id='add_cart'][contains(@title,'Перейти в корзину')]";

    public String getNameOfProduct() {
        String name = driver.findElement(By.xpath(locatorNameOfProduct)).getAttribute("content");
        return name;
    }

    public void addProductInArrayOfBasketProducts() {
        productsAddedInBasket.add(getNameOfProduct());
    }

    public void clickButtonAddToCart() {
        driver.findElement(By.xpath(locatorButtonAddToCart)).click();
    }

    public void clickButtonGoToCart() {
        WebDriverWait driverWait = new WebDriverWait(driver, Duration.ofMillis(10000), Duration.ofMillis(100));
        WebElement buttonGoToCart = driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(locatorButtonGoToCart)));

        buttonGoToCart.click();
    }
}
